package com.company;

public class RenderContentHandler extends RequestHandler {

    @Override
    public void handleRequest(WebRequest request) {
        if (request.getPath().equals("/dashboard")) { System.out.println("Status 200 : Dashboard content here"); }
        else if (request.getPath().equals("/home")) { System.out.println("Status 200 : Home content here"); }
        this.successorHandleRequest(request);
    }
}
