package com.company;

import java.util.ArrayList;

public class WebServer implements WebRequestObservable {

    private RequestHandler requestHandler;
    private ArrayList<WebRequestObserver> observers = new ArrayList<WebRequestObserver>();

    public WebServer(RequestHandler requestHandler) {
        this.requestHandler = requestHandler;
    }

    public String getRequest(WebRequest request) {
        requestHandler.handleRequest(request);
        notifyObservers(request);
        return "";
    }

    @Override
    public void attach(WebRequestObserver observer) {
        this.observers.add(observer);
    }

    @Override
    public void detach(WebRequestObserver observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers(WebRequest webRequest) {
        for (WebRequestObserver observer : this.observers) {
            observer.update(webRequest);
        }
    }
}
