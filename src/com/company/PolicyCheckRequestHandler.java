package com.company;

public class PolicyCheckRequestHandler extends RequestHandler {

    @Override
    public void handleRequest(WebRequest request) {
        if (request.getLoggedUser().isAdmin() || (!request.getLoggedUser().isAdmin() && request.getPath().equals("/home"))) {
            this.successorHandleRequest(request);
        }
        else {
            System.out.println("Status 403 : user is not authorized to access this content");
        }
    }
}
