package com.company;

public class ExistingContentCheckRequestHandler extends RequestHandler {

    public void handleRequest(WebRequest request) {
        if (request.getPath().equals("/dashboard") || request.getPath().equals("/home")) {
            this.successorHandleRequest(request);
        }
        else {
            System.out.println("Status 404 : Page missing");
        }
    }
}
